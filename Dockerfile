FROM centos:7
MAINTAINER Dmitriy Mamontov
RUN yum -y install epel-release; yum install -y nginx; yum clean all; systemctl enable nginx.service
EXPOSE 80
CMD ["/sbin/nginx"]
